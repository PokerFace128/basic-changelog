# basic-changelog

Assumes a changelog with h2's for version headings, corresponding to the release tags. Given the path to the changelog file, checks for tags that are not yet in the changelog and lists their commits, writes them back to the changelog file.

`changelog(path, [opts], cb)`

## Opts

`filterCommitsStartingWith` will filter out commits starting with the given strings

## Usage

```
const changelog = require('changelog')

changelog('./CHANGELOG.md', {filterCommitsStartingWith: ['release']}, err => {
    console.log(err)
})
```